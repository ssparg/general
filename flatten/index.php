<?php

$array = [
    'name' => [
        'Your name is required',
        'Your name cannot contain any numbers'
    ],
    'dob' => [
        'Your date of birth is required'
    ],
    'password' => [
        'Your password is required',
        'Your password must be 6 characters or more'
    ],
];

// Method 1

function flatten_array(array $items, array $flattened = []) {
    foreach ($items as $item) {
        if (is_array($item)) {
            $flattened = flatten_array($item, $flattened);
            continue;
        }

        $flattened[] = $item;
    }

    return $flattened;
}

// Method 2

$flattened = iterator_to_array(new RecursiveIteratorIterator(
    new RecursiveArrayIterator($array)
), false);


var_dump($flattened);