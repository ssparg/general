<?php

session_start();

// example for post

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_POST['_token']) || ($_POST['_token'] !== $_SESSION['_token'])) {
        die('Invalid');
    }
}
// PHP7 can use random_bytes
$_SESSION['_token'] = bin2hex(openssl_random_pseudo_bytes(16));