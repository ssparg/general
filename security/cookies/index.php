<?php

//http_only_cookie cannot be accessed from JS, think xss
// Dont store the following in cookies
// user id's as its stored as plain text
$week = new DateTime('+1 week');

// @param4 = path
// @param5 = domain
// @param5 = https
// @param6 = http
setcookie('key', 'value', $week->getTimestamp(), '/', null, null, true);

echo $_COOKIE['key'];
