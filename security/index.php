<?php

// Always end a file with .php, this ensures that it's not visible as plain text
// To disable page indexes within apache add Options -Indexes. It is recursive