<?php

// create password
// PASSWORD_DEFAULT or PASSWORD_BCRYPT
// The higher the cost the more processing time but stronger the password is
// echo password_hash('ilovecats33', PASSWORD_DEFAULT, ['cost' => 10]);

//verify password

$password = '$2y$10$Gh/IZ..yyVMNAmFWxexk0Oe3dk3ejaZ1jsU9.PsQsd9NfETrScZgm';
$submittedPassword = 'ilovecats33';

$result = password_verify($submittedPassword, $password);

var_dump($result);
