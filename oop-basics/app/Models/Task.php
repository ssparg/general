<?php

namespace Todo\Models;

use DateTime;

class Task
{
    protected $id;
    protected $complete = false;
    protected $description;
    protected $due;

    public function __get($property)
    {
        if (property_exists(Task::class, $property)) {
            return $this->property;
        }

        return null;
    }

    /**
     * Gets the value of complete.
     *
     * @return mixed
     */
    public function getComplete()
    {
        return (bool) $this->complete;
    }

    /**
     * Sets the value of complete.
     *
     * @param mixed $complete the complete
     *
     * @return self
     */
    public function setComplete($complete = true)
    {
        $this->complete = $complete;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Gets the value of due.
     *
     * @return mixed
     */
    public function getDue()
    {
        if (!$this->due instanceof DateTime) {
            return new DateTime($this->due);
        }

        return $this->due;
    }

    /**
     * Sets the value of due.
     *
     * @param mixed $due the due
     *
     * @return self
     */
    public function setDue(DateTime $due)
    {
        $this->due = $due;
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}