<?php

use Todo\Models\Task;
use Todo\TaskManager;
use Todo\Storage\MySqlDatabaseTaskStorage;

require 'vendor/autoload.php';

$db = new PDO('mysql:host=127.0.0.1;dbname=todo', 'root', '');

$storage = new MySqlDatabaseTaskStorage($db);
$manager = new TaskManager($storage);
$task = new Task;
// $task->setDescription('Sleep');
// $task->setDue(new DateTime('+11 hours'));
// var_dump($manager->addTask($task));
$task = $manager->getTask(6);
// $task->setDescription('Sleep');
// $task->setDue(new DateTime('+12 hours'));
// $task->setComplete(false);
// $manager->updateTask($task)
var_dump($task->description);

