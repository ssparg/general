<?php

use Carbon\Carbon;

require 'vendor/autoload.php';

Carbon::now(); //DateTime Obj
Carbon::today(); //Date Obj
Carbon::tomorrow(); //Tomorrow Date Obj
Carbon::yesterday(); //Yesterday Obj
Carbon::create(1985, 10, 21, 0, 0, 0); //DateTime Obj /Y/m/d H:i:s
Carbon::createFromDate(1985, 10, 21); //DateTime Obj /Y/m/d
Carbon::createFromTime(12, 10, 0); //DateTime Obj /Y/m/d H:i:s from Time
Carbon::createFromTimeStamp(54553233453); //DateTime Obj from timestamp
new Carbon('16th November 1989 10:00:00');
new Carbon('+ 2 days 6 hours');
$c = new Carbon('-2 days');
echo $c->toDateString() . "<br />";
echo $c->toTimeString() . "<br />";
echo $c->toDateTimeString() . "<br />";
echo $c->toRssString() . "<br />";
echo $c->toAtomString() . "<br />";
echo $c->format('d M Y') . "<br />";
echo $c->diffForHumans() . "<br />";
// var_dump($c);